using Microsoft.EntityFrameworkCore;
using UserIdentity.Models;

namespace UserIdentity.Context
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}